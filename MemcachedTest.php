<?php

$mc = new Memcached();
$mc->addServer("memcached", 11211);

$mc->set("foo", "Hello!");
$mc->set("bar", "Memcached...");
$mc->set('0', 'test');

$zero = $mc->get(0);
$arr = [
    $mc->get("foo"),
    $mc->get("bar"),
];

$mc->add('w1', 'test');
$mc->add('w1', 'test2');
$mc->add('w1', 'test3');

$str = $mc->get('w1');

$str;
$arr;

$zero;